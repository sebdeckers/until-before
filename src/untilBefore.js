export function untilBefore (pattern) {
  return this.includes(pattern)
    ? this.substring(0, this.indexOf(pattern))
    : this
}
